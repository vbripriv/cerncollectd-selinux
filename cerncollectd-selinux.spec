%global modulename cerncollectd

%if 0%{rhel} < 7
%define restorecon /sbin/restorecon
%else
%define restorecon /usr/sbin/restorecon
%endif


Name:       cerncollectd-selinux
Version:    2.4
Release:    1%{?dist}
Summary:    selinux policy addons for CERN collectd deployment
Group:      System Environment/Base
Vendor:     CERN
License:    GPL
URL:        https://gitlab.cern.ch/monitoring/cerncollectd-selinux
Source0:    %{name}-%{version}.tar.gz

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires:	checkpolicy
BuildRequires:	selinux-policy
BuildRequires:	bzip2
BuildRequires:  selinux-policy-devel
Requires:	selinux-policy-targeted
Requires:       collectd
Requires(post):   /usr/sbin/semodule
Requires(post):   %{restorecon}
Requires(postun): /usr/sbin/semodule

%description
SELinux policy addon for CERN collectd deployment

%prep
%setup -q

%build
(cd src/%{rhel}
 make -f %{_datadir}/selinux/devel/Makefile
 bzip2 *.pp

)

%install
%{__rm} -rf %{buildroot}
%{__mkdir} -p %{buildroot}%{_datadir}/selinux/targeted
install -m 0644 src/%{rhel}/%{modulename}.pp.bz2 %{buildroot}%{_datadir}/selinux/targeted/%{modulename}.pp.bz2

%clean
%{__rm} -rf %{buildroot}

%post
/usr/sbin/semodule -s targeted -i %{_datadir}/selinux/targeted/%{modulename}.pp.bz2 &> /dev/null || :
%{restorecon} -F %{_var}/log/collectd.log || :
/sbin/service collectd restart >/dev/null 2>&1 || :

%postun
if [ $1 -eq 0 ]; then
    /usr/sbin/semodule -s targeted -r %{modulename} &> /dev/null || :
    %{restorecon} -F %{_var}/log/collectd.log || :
fi

%files
%defattr(-,root,root,-)
%{_datadir}/selinux/targeted/%{modulename}.pp.bz2
%doc AUTHORS COPYING

%changelog
* Wed Aug 07 2019 - Gonzalo Menéndez Borge <gmenende@cern.ch> - 2.4-1
- [MONIT-2135] Grant access to read the rpm db and search its log

* Thu Apr 25 2019 - Nikolay Tsvetkov <n.tsvetkov@cern.ch> - 2.3-1
- [NOTICKET] Allow collectd to send a NULL signal (0) to any domain

* Tue Feb 26 2019 - Borja Garrido <borja.garrido.bear@cern.ch> - 2.2-1
- [NOTICKET] Emergency empty release to force reload everywhere

* Tue Dec 04 2018 - Borja Garrido <borja.garrido.bear@cern.ch> - 2.1-2
- [MONIT-1808] Fix broken requirement in SLC6

* Mon Dec 03 2018 - Borja Garrido <borja.garrido.bear@cern.ch> - 2.1-1
- [MONIT-1808] Update release following security advices

* Thu Nov 29 2018 - Borja Garrido <borja.garrido.bear@cern.ch> - 2.0-1
- Initial release
